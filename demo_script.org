#+TITLE: Demo Script
#+DATE: 19 November, 2017
#+AUTHOR: Raghav Mittal


** Zapper
   TEST: PASS
- Meeseva
[[http://tg.meeseva.gov.in/DeptPortal/UserInterface/LoginForm.aspx]]

** Modify content 
   TEST: /meeseva/ page crash, works fine with /mapsofindia/.

- Meeseva page
[[http://tg.meeseva.gov.in/DeptPortal/UserInterface/LoginForm.aspx]]

- India facts
[[http://www.mapsofindia.com/india/india-facts.html]]

** convert measurements(km to mile)
   TEST: PASS
   
- Facts about India
[[http://www.mapsofindia.com/india/india-facts.html]]

** convert number system(US to IN)
   TEST: PASS
   
- Facts about India
[[http://www.mapsofindia.com/india/india-facts.html]]

** convert date format
TEST: PASS

- Facts about India
[[http://www.mapsofindia.com/india/india-facts.html]]

** Phonetic
   TEST: PASS(Only for very common words)

- any =http= webpage
** Translate Text
   TEST: PASS

- Any =http= webpage
** convert currency(INR to USD, INR to EUR) 
   TEST: PASS
   
- XE Currency - information about Indian currency
[[http://www.xe.com/currency/inr-indian-rupee]]

** Switch CSS 
TEST
   - Theme 1 -- PASS no revert
   - Theme 2 -- PASS no revert
   - Theme 3 -- PASS no revert
- Any =http= webpage

** Translate text 
TEST: PASS

- Facts about India
[[http://www.mapsofindia.com/india/india-facts.html]]

** Visibility
TEST: PASS
- any =http= webpage

** Page stripper
   TEST: PASS, no revert

- any =http= webpage
